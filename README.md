# API Mashup - CFML Swarm Demo

This was presented at Into the Box 2019. Slides for the presentation are [here](https://slides.com/mjclemente/personal-docker-swarm-into-the-box-2019#/). It follows the basic structure laid out in the [Starter Swarm Template - CFML](https://gitlab.com/mjclemente/starter-swarm-cfml), but includes some more advanced services and integrations, including Nginx, Memcached, Portainer, FusionReactor, Traefik, and Consul.

I'll be adding instructions for setting up and using this, as this documentation is obviously insufficient.

*Note that this project is specifically tailored for the Lucee CFML engine. If you're interested in an API mashup using Adobe ColdFusion with Docker Swarm, check out this project:* [`api-mashup-coldfusion-swarm`](https://gitlab.com/mjclemente/api-mashup-coldfusion-swarm)