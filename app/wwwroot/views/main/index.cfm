﻿<cfoutput>
<div class="jumbotron mt-sm-5 p-4">
	<div class="row">

		<div class="col-md-12">
			<h1 class="display-3">#prc.welcomeMessage#</h1>
			<p class="lead">
        This is a CFML Swarm demo. The specifics of the API mashup, while fun, aren't the focus.
        <!--- https://blog.newrelic.com/engineering/container-orchestration-explained/ --->
			</p>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-lg-12">

		<section id="formarea">
		<div class="pb-2 mt-4 mb-2 border-bottom">
			<h2>
				Let's Add Some Text
			</h2>
		</div>
    #html.startForm(
        name		= "masher",
        action	= "masher.mix",
        noBaseURL = true
      )#
      <div class="form-group row">
        <label for="text" class="col-sm-2 col-form-label col-form-label-lg">Input Text</label>
        <div class="col-sm-10">
          <textarea class="form-control form-control-lg" id="text" name="text" rows="2" aria-describedby="passwordHelpInline" required></textarea>
          <small id="passwordHelpInline" class="text-muted">
            Please enter some text or a valid URL with content to be mashed up by the Giphy and Aylien APIs!
          </small>
        </div>
      </div>
      <div class="form-group row">
      <div class="col-sm-2"></div>
      <div class="col-sm-10">
       <button type="submit" class="btn btn-primary">Mash it up!</button>
      </div>
    </div>
    </form>
    </section>
    <cfif prc.keyExists( 'oMash' )>
      <section id="mashupResult">
        #getInstance( "messagebox@cbMessageBox" ).renderit()#
        <div class="row">
        <div class="col-lg-4">
          <h2>You entered: </h2>
          #html.makepretty( prc.oMash.getContent() )#
          <hr>
          <code>Hey, I'm #prc.oMash.getConfidence()#% sure that your input was #prc.oMash.getSentiment()# #prc.oMash.getEmoticon()#. Let's see what that looks like:</code>
          <br>
        </div>
        <div class="col-lg-6">#prc.oMash.renderGif()#</div>
        <div class="col-lg-2">
          <cfloop array="#prc.oMash.getTopHashTags()#" item="hashtag">
            <h4><span class="badge badge-primary">#hashtag#</span></h4>
          </cfloop>
        </div>

        </div>
        <hr>
        <a href="##formarea" class="btn btn-primary">Mash up something else!</a>

      </section>

    </cfif>

	</div>
</div>
</cfoutput>