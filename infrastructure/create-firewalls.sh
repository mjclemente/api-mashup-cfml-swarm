#!/bin/bash

## Set some colors for fun. Thanks to https://stackoverflow.com/questions/5947742/how-to-change-the-output-color-of-echo-in-linux
RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m'

## By default, we'll whitelist the current IP
LOCAL_PUBLIC_IP=$( dig @resolver1.opendns.com ANY myip.opendns.com +short )

: "${WHITELISTED_IP:=$LOCAL_PUBLIC_IP}"

## Tag this firewall will be applied to
: "${DO_FIREWALL_TAG:=swarm-firewall}"

## Provide prompt to make sure that user knows what they're doing
while true; do
    read -p "
This script will create DigitalOcean firewalls for a Docker Swarm deployment.
You should review the actual script before running it, to confirm that you understand what it is doing.
Here's a summary:

- The firewalls will be tagged with $(tput bold)${DO_FIREWALL_TAG}$(tput sgr0)
- All Droplets with this tag will be added to the firewall automatically.
- The following IP will be given whitelisted access to certain ports: $(tput bold)${WHITELISTED_IP}$(tput sgr0)
- A number of ports will be opened to support standard access, Docker Swarm, Consul, and Portainer.

Do you wish to continue? " yn
    case $yn in
        [Yy]* ) printf "\n${GREEN}%s${NC}\n" "Proceeding with firewall creation."; break;;
        [Nn]* ) printf "\n${RED}%s${NC}\n" "Firewall creation cancelled."; exit;;
        * ) printf "\n%s\n" "Please answer yes or no.";;
    esac
done

## Need to make sure the tag exists. If not, create it.
{
  doctl compute tag get "${DO_FIREWALL_TAG}"
} || {
  doctl compute tag create "${DO_FIREWALL_TAG}"
}

## Whitelist
doctl compute firewall create --name Whitelist \
  --inbound-rules "protocol:icmp,address:$WHITELISTED_IP protocol:tcp,ports:8080,address:$WHITELISTED_IP protocol:tcp,ports:8443,address:$WHITELISTED_IP protocol:tcp,ports:9000,address:$WHITELISTED_IP" \
  --tag-names ${DO_FIREWALL_TAG}

## Standard Rules - ports 22, 80, 443
doctl compute firewall create --name Standard \
  --inbound-rules "protocol:tcp,ports:22,address:0.0.0.0/0,address:::/0 protocol:tcp,ports:80,address:0.0.0.0/0,address:::/0 protocol:tcp,ports:443,address:0.0.0.0/0,address:::/0" \
  --outbound-rules "protocol:icmp,address:0.0.0.0/0,address:::/0 protocol:tcp,ports:all,address:0.0.0.0/0,address:::/0 protocol:udp,ports:all,address:0.0.0.0/0,address:::/0" \
  --tag-names ${DO_FIREWALL_TAG}

## Swarm Rules
doctl compute firewall create --name Swarm \
  --inbound-rules "protocol:icmp,tag:network-internal protocol:tcp,ports:2377,tag:network-internal protocol:tcp,ports:7946,tag:network-internal protocol:udp,ports:4789,tag:network-internal protocol:udp,ports:7946,tag:network-internal" \
  --tag-names ${DO_FIREWALL_TAG}

## Portainer Rule
doctl compute firewall create --name Portainer \
  --inbound-rules "protocol:tcp,ports:9000,tag:network-internal protocol:tcp,ports:9001,tag:network-internal" \
  --tag-names ${DO_FIREWALL_TAG}

## Consul Rule
doctl compute firewall create --name Consul \
  --inbound-rules "protocol:tcp,ports:8300,tag:network-internal protocol:tcp,ports:8301,tag:network-internal protocol:tcp,ports:8302,tag:network-internal protocol:tcp,ports:8500,tag:network-internal protocol:tcp,ports:8501,tag:network-internal protocol:tcp,ports:8502,tag:network-internal protocol:tcp,ports:8600,tag:network-internal protocol:udp,ports:8600,tag:network-internal" \
  --tag-names ${DO_FIREWALL_TAG}
