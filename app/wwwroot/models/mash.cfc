component accessors="true"{

  property name="aylien" inject="aylien@ayliencfc";
  property name="giphy" inject="giphy@giphycfc";

	// Properties
	property name="content" type="string" default="";
  property name="hashtags" type="array" default="";
  property name="sentiment" type="string" default="";
  property name="confidence" type="string" default="";
  property name="emoticon" type="string" default=":-|";
  property name="gif" type="string";

	/**
	 * Constructor
	 */
	mash function init(){
		variables.hashtags = [];
		return this;
  }

  function up() {
    var errors = [];

    var sentimentResponse = false
      ? variables.aylien.sentiment( url = variables.content )
      : variables.aylien.sentiment( text = variables.content, mode = mode() );

    if( sentimentResponse.statusCode != 200 ) {
      errors.append( "Sentiment Analysis API returned a #sentimentResponse.statusCode# response. Sorry." );
    } else {

      setSentiment( sentimentResponse.data.polarity );

      if ( variables.sentiment == 'positive' ) setEmoticon( ':-)' );
      if ( variables.sentiment == 'negative' ) setEmoticon( ':-(' );

      setConfidence( numberformat( ( sentimentResponse.data.polarity_confidence * 100 ), '__.00' ) );

    }

    var hashtagResponse = isUrl()
      ? variables.aylien.hashtags( url = variables.content )
      : variables.aylien.hashtags( text = variables.content );

    if( hashtagResponse.statusCode != 200 )
      errors.append( "Hashtag Analysis API returned a #hashtagResponse.statusCode# response. Sorry." );
    else
      setHashtags( hashtagResponse.data.hashtags );

    var giphyResponse = variables.giphy.gifsTranslateGet( s = variables.content );

    if( giphyResponse.statusCode != 200 )
      errors.append( "Giphy API returned a #giphyResponse.statusCode# response. Sorry." );
    else if( !giphyResponse.data.data.len() )
      errors.append( "Giphy API couldn't find anything. Sorry." );
    else
      setGif( giphyResponse.data.data.images.original.url );

    return errors;
  }

  string function renderGif() {
    return variables.gif.len() ? '<img src="#variables.gif#" alt="API Mashup result gif" />' : '';
  }

  array function getTopHashTags() {
    if( variables.hashtags.len() > 3 )
      return variables.hashtags.slice( 1, 3);
    else
      return variables.hashtags;
  }

  /**
  * https://cflib.org/udf/isURL
  */
  boolean function isUrl() {
    var URLRegEx = "(?i)\b((?:[a-z][\w-]+:(?:/{1,3}|[a-z0-9%])|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:'"".,<>?«»“”‘’]))";
    return isValid("regex", variables.content, URLRegex);
  }

  string function mode() {
    return variables.content.len() > 288 ? 'document' : 'tweet';
  }

}