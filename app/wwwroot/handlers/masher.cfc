/**
* I am a new handler
*/
component{

  property name="messagebox" inject="MessageBox@cbmessagebox";
  property name="antisamy" inject="antisamy@cbantisamy";

	// REST Allowed HTTP Methods Ex: this.allowedMethods = {delete='POST,DELETE',index='GET'}
  this.allowedMethods = {};


  function mix(event,rc,prc){
    param name="rc.text" default="";

    rc.content = trim( antisamy.clean( rc.text ) );

    if ( !rc.content.len() ) {
      messagebox.warn( "Looks like no valid text was entered. Please try again." );
      relocate( uri = "/" );
    }

    var oMash = populateModel( "mash" );
    var errors = oMash.up();

    if( errors.len() )
      messagebox.warn( "Sorry. There were some issues while creating this mashup." );
    else
      messagebox.success( "Mashup created successfully. Take a look!" );

    flash.put( name="oMash", value=oMash, inflateToRC=false, inflateTOPRC=true );
		relocate( uri = "/##mashupresult" );
	}

}
